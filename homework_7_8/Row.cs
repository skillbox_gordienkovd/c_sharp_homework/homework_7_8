using System;

namespace homework_7_8
{
    public class Row
    {
        public long Id { get; set; }

        public string Topic { get; set; }

        public string Body { get; set; }

        public string CreatedBy { get; set; }

        public DateTime Created { get; set; }

        public override string ToString()
        {
            return $"{{Id: {Id}, Topic: {Topic}, Body: {Body}, CreatedBy: {CreatedBy}, Created: {Created}}}";
        }
    }
}