using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace homework_7_8
{
    public class Organizer
    {
        public List<Row> Rows { get; set; } = new();
        
        public void Load()
        {
            try
            {
                Rows = JsonSerializer.Deserialize<Organizer>(File.ReadAllText(@"d:\organizer.json"))?.Rows;
            }
            catch (Exception e)
            {
                Console.Write("Файл БД отутствует.");
            }
        }

        public void Save()
        {
            File.WriteAllText(@"d:\organizer.json", JsonSerializer.Serialize(this));
        }

        public void AddRow(Row row)
        {
            Rows.Add(row);
        }

        public void AddRows(IEnumerable<Row> rows)
        {
            Rows.AddRange(rows);
        }

        public void DeleteRow(Row row)
        {
            Rows.Remove(row);
        }

        public void DeleteRows(IEnumerable<Row> rows)
        {
            foreach (var row in rows)
            {
                Rows.Remove(row);
            }
        }

        public void DeleteById(long id)
        {
            var rows = Rows.Where(row => row.Id == id);
            DeleteRows(rows);
        }
        
        public void DeleteByTopic(string topic)
        {
            var rows = Rows.Where(row => row.Topic == topic);
            DeleteRows(rows);
        }
        
        public void DeleteByBody(string body)
        {
            var rows = Rows.Where(row => row.Body == body);
            DeleteRows(rows);
        }
        
        public void DeleteByCreatedBy(string createdBy)
        {
            var rows = Rows.Where(row => row.CreatedBy == createdBy);
            DeleteRows(rows);
        }
        
        public void DeleteByCreatedCreated(DateTime created)
        {
            var rows = Rows.Where(row => row.Created == created);
            DeleteRows(rows);
        }

        public IEnumerable<Row> FindByCreatedBetween(DateTime from, DateTime to)
        {
           return Rows.Where(row => row.Created >= from).Where(row => row.Created <= to);
        }

        public IEnumerable<Row> OrderById()
        {
            return Rows.OrderBy(row => row.Id);
        }

        public IEnumerable<Row> OrderByTopic()
        {
            return Rows.OrderBy(row => row.Topic);
        }
        
        public IEnumerable<Row> OrderByBody()
        {
            return Rows.OrderBy(row => row.Body);
        }
        
        public IEnumerable<Row> OrderByCreatedBy()
        {
            return Rows.OrderBy(row => row.CreatedBy);
        }
        
        public IEnumerable<Row> OrderByCreated()
        {
            return Rows.OrderBy(row => row.Created);
        }

        public override string ToString()
        {
            return string.Join(",\n", Rows);
        }
    }
}