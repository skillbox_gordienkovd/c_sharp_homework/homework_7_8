﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace homework_7_8
{
    internal class Program
    {
        private static readonly Random random = new();

        private static void Main(string[] args)
        {
            var organizer = new Organizer();
            GenerateAndSaveDb(organizer);
            Console.Write("Сгенерировано автоматически:\n" + organizer);
            var rows = organizer.FindByCreatedBetween(DateTime.Now.AddDays(-2), DateTime.Now);
            var enumerable = rows.ToList();
            Console.Write($"\nОтфильтровано по дате создания:\n{string.Join(",\n", enumerable)}" );
            organizer.DeleteRows(enumerable);
            organizer.Save();
            organizer.Load();
            Console.Write("\nПосле удаления:\n" + organizer);
            organizer.Rows[0].Topic = "Измененная тема";
            organizer.Save();
            organizer.Load();
            Console.Write("\nПосле изменения темы:\n" + organizer);
        }

        private static void GenerateAndSaveDb(Organizer organizer)
        {
            var rows = new List<Row>();

            for (var i = 0; i < 10; i++)
            {
                var row = new Row
                {
                    Id = i,
                    Topic = RandomString(random.Next(5, 10)),
                    Body = RandomString(random.Next(10, 30)),
                    CreatedBy = RandomString(random.Next(6, 12)),
                    Created = DateTime.Now.AddDays(-i)
                };
                rows.Add(row);
            }

            organizer.AddRows(rows);
            organizer.Save();
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}